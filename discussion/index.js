// console.log("Hello world");

// Arrays
// Arrays are used to store multiple related data/value in a single variable
// It is created/declared using [] also known as 'Array Literals'

let hobbies = ["Play video games", "Read a book", "Listen to music"];

// Arrays make it easy to manage, manipulate a set of data. Arrays have different methods/functions that allow us to manage our array.

// Methods are functions associated with an object
// Because Arrays are actually a special type of object

console.log(typeof hobbies);

let grades = [75.4,98.5,90.12,91.50];
const planets = ["Mercury","Venus","Mars","Earth"];

// Arrays as a best practice contain values of the same type.

let arraySample = ["Saitama", "One Punch Man", 25000, true];

// However, since there are no problems to creating arrays like this, you may encounter exceptions to this rule in the future and in fact in other JS libraries or framework.

// Array as a collection of data, has methods to manipulate and manage the array . Having values with different data types might interfere or conflict with the methods of an array.

let dailyRoutine = ["Shower","Eat","Code","Watch Anime","Sleep"];

let capitalCities = ["Manila","Tokyo","Beijing","Seoul"]

console.log(dailyRoutine);
console.log(capitalCities);

// Each item in an array is called an element.

// Array as a collection of data, as a convention, its name is usually plural.

// We can also add values of variabbles as elements in an array.

let username1 = "fighter_smith1";
let username2 = "georgeKyle5000";
let username3 = "white_night";

let guildMembers = [username1,username2,username3];

console.log(guildMembers);

// .lengthy property
// The .lengthy property of an array tells us about the number of elements in the array.
// It can actually also be set and manipulated.
// The .length property of an array a number type

console.log(dailyRoutine.length);
console.log(capitalCities.length);

// In fact, even strings have a .length property, which tells us the number of characters in a string.
// strings are able to use some array methods and properties.
// Whitespaces are counted as characters

let fullName = "Randy Orton";
console.log(fullName.length);

// We can manipulate a .length property of an array. Being that .length property is a number that tells us the total number of elements in an array, we can also delete the last item in a array by manipulating the .length property.

dailyRoutine.length = dailyRoutine.length-1;
console.log(dailyRoutine.length);
console.log(dailyRoutine);

// We could also decrement the .length property of an array. It will product the smae result that it will delete the last item in the array.

capitalCities.length--;
console.log(capitalCities);

// If we can shorten the array by updating the length property, can we also lengthen or add using the same trick?

let theBeatles = ["John","Paul","Ringo","George"];
theBeatles.length++;
console.log(theBeatles);

// Accessing the Elements of the Array
// Accessing array element is one of the more common task we do with an array.
// This can be done throught the use of array indices.
// Array elements are ordered according to index.

// In programming language such as Javascript, arrays start from 0.
// The first element in an array is at index 0.

console.log(capitalCities);

// If we want to access a particular item in the array, we can do so with array indices. Each item are ordered according to their index. We can locate items in an array vie their index

// Syntax: arrayName[index]
// Note: index are number types.

console.log(capitalCities[0]);

let lakersLegends = ["Kobe","Shaq","Lebron","Magic","Kareem"];
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

// We can also save/strore a particular array element in a variable

let currentLaker = lakersLegends[2];
console.log(currentLaker);

// We can also update/reassign the array element using their index.

lakersLegends[2]="Pau Gasol";
console.log(lakersLegends);

/*
	Mini Activity
	
	Update/reassign the last two items in the array with your own personal/favorite

	log the favoriteFoods array with its updated values

*/

let favoriteFoods = [
	
	"Tonkatsu",
	"Adobo",
	"Hamburger",
	"Sinigang",
	"Pizza"
];

favoriteFoods[3]="Taco Birria";
favoriteFoods[4]="Bulalo";
console.log(favoriteFoods);

// What if we do not know the total number of items in the array? Or what if the array if constantly being added into?

// We could consistently access the last item in our array by adding the .length property value minus 1 as the index

// the index to the last item is arrayName.length-1 because the index starts at 0

console.log(favoriteFoods[favoriteFoods.length-1]);

let bullsLegends = ["Jordan","Pippen","Rodman","Rose","Scalabrine"]
console.log(bullsLegends[bullsLegends.length-1]);

// Add items in an array

// Using our indices, we can also add items in our array.

let newArr = [];
console.log(newArr);
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Aerith Gainsborough";
console.log(newArr);


newArr[newArr.length-1] = "Tifa Lockhart";
console.log(newArr);


// newArr[2]
newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

// Looping over an Array

// We can loop over an array and iterate all items in the array.
// Set the counter as the index and set a condition that as the current index iterated is less than the length of the array, we will run the loop.
// It is set this way because index starts at 0

// Loop over and display all items in the newArr array:

for(let index = 0; index < newArr.length; index++){

	// so that the current counter will be the index to access the item in the array
	console.log(newArr[index]);

}

let numArr = [5,12,30,46,40];

// check each item in the array if they are divisible by 5 or not.

for(let index = 0; index < numArr.length; index++){

	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	} else {
		console.log(numArr[index] + " is not divisible by 5");
	}
}

// Multidimensional Arrays

// Multidimensional Arrays are arrays that contain other arrays

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);

// Accessing items in a multidimensional array is different that one dimensional array

/* 	
	accessing multidimensional array:
	
	1. access which array the item is in 

	2. then add its location in its array 
*/
console.log(chessBoard[1][5]);

console.log(chessBoard[7][0]);
console.log(chessBoard[5][7]);